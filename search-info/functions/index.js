const firebase = require("firebase");
const admin = require('firebase-admin');
const cors = require('cors')({ origin: true });
const functions = require('firebase-functions');

// Required for side-effects
require("firebase/firestore");

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: "AIzaSyAo2-zSRKdhxbkEWmdkCf2R0V_ZuPGZ3pY",
    authDomain: "misdocumentosafiliado.firebaseapp.com",
    projectId: "misdocumentosafiliado"

});

var db = firebase.firestore();

exports.buscarFiltros = functions.https.onRequest((req, res) => {
    cors(req, res, () => {


        const { body } = req;

        /*
        if (req.method !== "GET") {
            res.status(400).send('Please send a get request');
            return;
        }
        */

        /*
       const isValidMessage = body.rut;

       if (!isValidMessage) {

           var respuesta = {
               error: true,
               codigo: 400,
               mensaje: 'parametros de entrada invalidos'
           }

           return res.send({ respuesta });
       }
       */

        /*
                var _rut = req.query.rut;
                var _motivo = req.query.motivo;
                var _fecha = req.query.fecha;
                var _estado = req.query.estado;
        */

        var _rut = body.rut;
        var _motivo = body.motivo;
        var _fecha = body.fecha;
        var _estado = body.estado;

        console.log(_rut + " " + _motivo + " " + _fecha)

        documento = {
            archivo: "",
            fecha: "",
            fileName: "",
            tipoDocDescrip: "",
            tipoDocumento: ""
        }

        usuario = {
            estado: "",
            fecha: "",
            motivoEnvio: "",
            estado: "",
            infoComplementaria: "",
            documentos: [documento]

        };

        var respuesta = new Array();
        var _busca = "motivoEnvio,'==," + _motivo;
        var _tipoBusca = "motivoEnvio";

        var _tipoBusca = "estado";
        console.log(_motivo);
        console.log(_rut);

        var citiesRef = db.collection("solicitudes")

        var query1 = citiesRef;
        var query2 = "";

        if (!Object.keys(_rut).length == 0) {
            console.log("entro rut " + _rut);
            query2 = query1.where("rutAfiliado", "==", _rut);
        }

        if (!Object.keys(_motivo).length == 0 && Object.keys(query2).length == 0) {
            console.log("entro motivo 1 " + _motivo);
            query2 = query1.where("motivoEnvio", "==", _motivo);
        } else if (!Object.keys(_motivo).length == 0 && Object.keys(query2).length > 0) {
            console.log("entro motivo 2 " + _motivo);
            query2 = query2.where("motivoEnvio", "==", _motivo);
        }

        if (!Object.keys(_estado).length == 0 && Object.keys(query2).length == 0) {
            console.log("entro estado " + parseInt(_estado));
            query2 = query1.where("estado", "==", parseInt(_estado));
        } else if (!Object.keys(_estado).length == 0 && Object.keys(query2).length > 0) {
            console.log("entro estado 2 " + parseInt(_estado));
            query2 = query2.where("estado", "==", parseInt(_estado));
        }

        if (!Object.keys(_fecha).length == 0 && Object.keys(query2).length == 0) {

            console.log("entro fecha " + _fecha);

            var fechaIn = _fecha;
            var fechaInFormat = new Date(fechaIn).getTime();

            var fechaMAx = new Date(fechaIn);
            fechaMAx.setDate(fechaMAx.getDate() + 1);
            var fechaMaxFormat = new Date(fechaMAx).getTime();

            query2 = query1.where("createdAt", ">=", fechaInFormat).where("createdAt", "<=", fechaMaxFormat);

        } else if (!Object.keys(_fecha).length == 0 && Object.keys(query2).length > 0) {
            console.log("entro fecha 2 " + _fecha);
            query2 = query2.where("createdAt", ">=", fechaInFormat).where("createdAt", "<=", fechaMaxFormat)
        }

        if (Object.keys(query2).length == 0) {
            console.log("no entro");
            query2 = citiesRef;
        }


        query2.get().then(function(querySnapshot) {
                querySnapshot.forEach(function(doc) {
                    const post = doc.data() || {};

                    usuario = {
                        estado: post.estado || {},
                        fecha: post.fecha || {},
                        fechaNum: post.createdAt || {},
                        motivoEnvio: post.motivoEnvio || {},
                        rutAfiliado: post.rutAfiliado || {},
                        infoComplementaria: post.infoComplementaria || {},
                        documentos: post.documentos
                    }

                    respuesta.push(usuario);

                    var docs = querySnapshot.docs.map(doc => doc.data());

                });

                return res.send({ respuesta });

            })
            .catch(function(error) {

                var respuesta = {
                    error: true,
                    codigo: 500,
                    mensaje: 'error al filtart la informacion'
                }

                return res.send({ respuesta });

            });

    });
});