# Clone this repository
$ git clone https://gitlab.com/RobertoMaldonado_s4t/cclafiltroinformacion
.git

# Go into the repository
$ cd cclafiltroinformacion/search-info/functions

# Install dependencies
$ npm install


# Run the app
$ firebase serve



# not use
$ firebase use --add misdocumentosafiliado


